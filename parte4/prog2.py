"""
De la galería de productos, el usuario introducirá el código y el número de unidades del 
producto que desea comprar. El programa determinará el total a pagar, como una factura.
Una variante a este ejercicio que lo haría un poco más complejo sería dar la posibilidad de 
seguir ingresando diferentes códigos de productos con sus respectivas cantidades, y cuando el 
usuario desee terminar el cálculo de la factura completa con todas sus compras. Te animas??
"""

galeria = """
    ELIJA EL PRODUCTO DESEADO:
    
    PRODUCTO                    CODIGO

    CAMISA....................... 1
    CINTURON..................... 2
    ZAPATOS...................... 3
    PANTALON..................... 4
    CALCETINES................... 5
    FALDAS....................... 6
    GORRAS....................... 7
    SUETER....................... 8
    CORBATA...................... 9
    CHAQUETA..................... 10

"""
salir = 0

catalogo = {1: 10.0, 2: 20.0, 3: 25.0, 4: 30.0, 5: 35.0, 6: 40.0, 7: 45.0, 8: 50.0, 9: 55.0, 10: 60.0}

while(salir != 1):
    print(f"{galeria}")
    codigo = int(input("INTRODUSCA EL CODIGO: "))
    precio = catalogo.get(codigo)
    print("\nEL PRECIO ES: $", precio)
    numeroUnidades = int(input("INTRODUSCA EL NUMERO DE UNIDADES: "))
    pagoTotal = lambda x, y : x * y
    print("EL TOTAL A PAGAR ES $", pagoTotal(catalogo.get(codigo), numeroUnidades))
    salir = int(input("\nSI DESEA SALIR PRESIONE 1 O DE LO CONTRARIO PRESIONE OTRO NUMERO: "))

else:
    print("Hasta luego ... ")