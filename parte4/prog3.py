"""
Este programa muestra primero el listado de categorías de películas y pide al usuario que
introduzca el código de la categoría de la película y posterior a ello pide que el usuario
introduzca el número de días de atraso, y así se muestra al final el total a pagar.
"""

listado = """
CATEGORIA           PRECIO          CODIGO          RECARGO/DIA DE RETRASO

FAVORITOS           $2.50           1                       $0.50
NUEVOS              $3.00           2                       $0.75
ESTRENOS            $3.50           3                       $1.00
SUPER ESTRENOS      $4.00           4                       $1.50

"""

salir = 0

catalogo = {1: 0.5, 2: 0.75, 3: 1, 4: 1.5}
precio = {1: 2.5, 2: 3.0, 3: 3.5, 4: 4.0}

while(salir != 1):
    print(f"{listado}")
    codigo = int(input("INTRODUSCA EL CODIGO DE LA CATEGORIA DE LA PELICULA: "))
    dias = int(input("INTRODUSCA EL NUMERO DE DIAS DE ATRASO EN LA DEVOLUCION: "))

    pagoTotal = lambda x, y, z: x * y + z
    print("EL TOTAL A PAGAR ES $", pagoTotal(catalogo.get(codigo), dias, precio.get(codigo)))
    salir = int(input("\nSI DESEA SALIR PRESIONE 1 O DE LO CONTRARIO PRESIONE OTRO NUMERO: "))

else:
    print("Hasta luego ... ")