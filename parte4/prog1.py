"""
Este programa pide primeramente la cantidad total de compras de una persona.
Si la cantidad es inferior a $100.00, el programa dirá que el cliente no aplica a la promoción.
Pero si la persona ingresa una cantidad en compras igual o superior a $100.00,
el programa genera de forma aleatoria un número entero del cero al cinco.

Cada número corresponderá a un color diferente de cinco colores de bolas que hay para determinar
el descuento que el cliente recibirá como premio. Si la bola aleatoria es color blanco,
no hay descuento, pero si es uno de los otros cuatro colores, sí se aplicará un descuento
determinado según la tabla que  aparecerá, y ese descuento se aplicará sobre el total de compra
que introdujo inicialmente el usuario, de manera que el programa mostrará un nuevo valor a pagar
luego de haber aplicado el descuento.
"""

menu = """
SU GASTO IGUALA O SUPERA LOS $ 100.00 POR LO TANTO PARTICIPA EN LA PROMOCION

COLOR                   DESCUENTO

BOLA BLANCA             NO TIENE
BOLA ROJA               10 %
BOLA AZUL               20 %
BOLA VERDE              25 %
BOLA AMARILLA           50 %
"""
import random

DESC = [0, 0.1, 0.2, 0.25, 0.5]
BOLAS = ["BLANCA", "ROJA", "AZUL", "VERDE", "AMARILLO"]

def aplicarDescuento(gasto):
    if gasto >= 100:
        index = random.choice(range(len(DESC)))
        valorDesc = DESC[index]

        if index != 0:
            gasto = gasto - (gasto*valorDesc)
            return f"{menu}\nALEATORIAMENTE OBTUVO UNA BOLA {BOLAS[index]}\nFELICIDADES A GANADO UN {valorDesc*100} % DE DESCUENTO\nSU NUEVO TOTAL A PAGAR ES {gasto}"
        else:
            return f"{menu}\nALEATORIAMENTE OBTUVO UNA BOLA {BOLAS[0]}\nNO UBTUNO NINGUN DESCUENTO\nSU PAGO ES DE {gasto}"
    else:
        return f"SU COMPRA ES DE {gasto} Y NO APLICA PROMOCION"

gasto = int(input("INSTRODUCA LA CANTIDAD TOTAL DE LA COMPRA: "))

print(aplicarDescuento(gasto))