"""Definir una lista con un conjunto de nombres, imprimir la cantidad de comienzan con la letra a."""

palabras = ["Marcia", "Cristina", "Marili", "andy", "arla"]

def palabrasConA(lista, funcionLambda):
    cont = 0
    for palabra in lista:
        if funcionLambda(palabra):
            cont += 1
    return cont

print(palabrasConA(list(palabras), lambda x : x[0] == "a"))