"""
Definir una tupla con 10 edades de personas.
Imprimir la cantidad de personas con edades superiores a 20.
Puedes variar el ejercicio para que sea el usuario quien ingrese las edades.
"""
print("Imprimir cantidad de personas con edades mayores a 20")

edadesPersonas = (35,22,18,21,6,8,34,1,11,10)

def imprimir(lista, funcionLambda):
    cont = 0
    for edad in lista:
        if funcionLambda(edad):
            cont += 1
    return cont

print(imprimir(list(edadesPersonas), lambda x : x > 20))