"""Escribir una función mas_larga() que tome una lista de palabras y devuelva la mas larga."""

def mas_larga(lista):
    indexPalabraMasLarga = 0
    aux = 0
    for i in range(len(lista)):
        if aux < len(lista[i]):
            aux = len(lista[i])
            indexPalabraMasLarga = i
    return lista[indexPalabraMasLarga]

print(mas_larga(["Marcia", "Cristina", "Marili", "Sandy", "Karla"]))