"""Escribir una función max_in_list() que tome una lista de números
    y devuelva el mas grande."""

def max_in_list(lista):
    lista.sort(reverse=True)
    return lista[0]

print(max_in_list([1,33,23,53,13,1,8]))