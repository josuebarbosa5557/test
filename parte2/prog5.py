"""Construir un pequeño programa que convierta números binarios en enteros."""

def toBinary(numero):
	binary = bin(numero)[2:]
	return binary

number = 10
binaryNumber = str(toBinary(number))
print(binaryNumber)