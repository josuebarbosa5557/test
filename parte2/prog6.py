"""
Escribir un pequeño programa donde:
- Se ingresa el año en curso.
- Se ingresa el nombre y el año de nacimiento de tres personas.
- Se calcula cuántos años cumplirán durante el año en curso.
- Se imprime en pantalla.
"""

yearEnCurso = int(input("Ingresa el año en curso: "))
print("\nIngresa el nombre y el año de nacimiento de tres personas\n")

def agregarPersona(n):
    diccionario = {}
    for i in range(n):
        nombrePersona = input("Nombre de la persona " + str(i+1) + " : ")
        anioNacimiento = int(input("Anio de nacimiento de la persona " + str(i+1) + " : "))
        diccionario[nombrePersona] = anioNacimiento
    print("")
    for clave, valor in diccionario.items():
        print(clave, "su edad en", yearEnCurso, "es:", abs(valor-yearEnCurso))
    
agregarPersona(3)