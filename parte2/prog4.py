"""Escribir un programa que le diga al usuario que ingrese una cadena.
    El programa tiene que evaluar la cadena y decir cuantas letras mayúsculas tiene."""

def evaluarCadena(string):
    totalMayuscula, totalMinusculas = 0, 0
    lista = list(string)
    for i in range(len(string)):
        if lista[i].isupper():
            totalMayuscula += 1
        elif lista[i].islower():
            totalMinusculas += 1
    return "mayusculas: ", totalMayuscula, "minusculas: ", totalMinusculas

print(evaluarCadena("CRISTIna"))