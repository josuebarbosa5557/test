"""Crear una función contar_vocales(), que reciba una palabra y cuente cuantas
    letras "a" tiene, cuantas letras "e" tiene y así hasta completar todas las vocales."""

def contar_vocales(palabra, funcionLambda):
    cont = 0
    for p in palabra:
        if funcionLambda(p):
            cont += 1
    return cont

print(contar_vocales(list("Cristina") , lambda x : ord(x) in [65, 69, 73, 79, 85, 97, 101, 105, 111, 117] ))