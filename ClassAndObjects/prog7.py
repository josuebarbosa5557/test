"""
Escribir una clase en python que calcule pow(x, n)
x = es la base
n = es el exponente
"""

class Potencia:
    def __init__(self, x, n):
        self.x = x
        self.n = n

    def calcularPow(self):
        return self.x**self.n

newCalculo = Potencia(2, -3)
print(newCalculo.calcularPow())