"""
Escribir una clase en python que encuentre los 3 elementos que sumen 0 a partir de números reales
Entrada: [-25, -10, -7, -3, 2, 4, 8, 10]
Salida: [[-10, 2, 8], [-7, -3, 10]]
"""
from itertools import combinations

class Encontrar:
    def __init__(self, matriz):
        self.matriz = matriz

    def suma(self):
        miLista = list(combinations(self.matriz, 3))
        miSublista = []

        for numero in miLista:
            if sum(numero) == 0:
                miSublista.append(list(numero))
        return miSublista

matriz = [-25, -10, -7, -3, 2, 4, 8, 10]
encontrarCero = Encontrar(matriz)
print((encontrarCero.suma()))