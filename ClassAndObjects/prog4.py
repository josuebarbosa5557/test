"""
Escribir una clase en python que obtenga todos los posibles subconjuntos únicos de un conjunto
de números enteros distintos.
Entrada: [4, 5, 6]
Salida: [[], [6], [5], [5, 6], [4], [4, 6], [4, 5], [4, 5, 6]]
"""
from itertools import chain, combinations

class Subconjunto:
    """
    aqui
    """
    def __init__(self, numeros):
        self.numeros = numeros

    def powerset(self):
        s = list(self.numeros)
        return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

numeros = [4, 5, 6]

newSubCon = Subconjunto(numeros)
print(list(newSubCon.powerset()))