"""
Escribir una clase en python que encuentre un par de elementos (índice de los números)
de una matriz dada cuya suma es igual a un número de destino especifico.
Entrada: numeros = [10,20,10,40,50,60,70], objetivo=50
Salida: 3, 4
"""

class Encontrar:
    def __init__(self, matriz, numero):
        self.matriz = matriz
        self.numero = numero

    def suma(self):
        longMatriz = len(self.matriz)

        for i in range(longMatriz):
            for j in range(longMatriz):
                if self.matriz[i] == self.numero or self.matriz[j] == self.numero:
                    continue
                if i == j:
                    continue
                if self.matriz[i] + self.matriz[j] == self.numero:
                    # return self.matriz[i], self.matriz[j]
                    return i+1, j+1
                    break
        else:
            return "Ninguna suma de dos numeros satisface el numero especifico"

matriz = [10,20,10,40,50,60,70]
numero = 50
encontrarPar = Encontrar(matriz, numero)
print(encontrarPar.suma())