"""
Escribir una clase en python con 2 métodos: get_string y print_string. get_string acepta una
cadena ingresada por el usuario y print_string imprime la cadena en mayúsculas.
"""

class ManejoDeCadenas:
    def __init__(self, cadena):
        self.cadena = cadena
    
    def get_string(self):
        self.cadena = input("Ingrese una cadena: ")
        return self.cadena
    
    def print_string(self):
        return self.cadena.upper()

cadena = ""
newCadena = ManejoDeCadenas(cadena)
newCadena.get_string()
print(newCadena.print_string())