"""
Escribir una clase en python que revierta una cadena de palabras
Entrada: "Mi Diario Python"
Salida: "Python Diario Mi"
"""

class Revertir:
    def __init__(self, cadena):
        self.cadena = cadena

    def reversa(self):
        lista = list(self.cadena.split(" "))
        lista = lista[::-1]
        lista = " ".join(lista)
        return lista

cadena = "Mi Diario Python"
newCadena = Revertir(cadena)
print(newCadena.reversa())