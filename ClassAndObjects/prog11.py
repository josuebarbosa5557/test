"""
Escribir una clase en python llamada circulo que contenga un radio, con un método que devuelva
el área y otro que devuelva el perímetro del circulo.
"""
class Circulo:
    def __init__(self, radio):
        self.radio = radio

    def obtenerArea(self):
        return format(3.141592653589*(self.radio**2), ".3f")

    def obtenerPerimetro(self):
        return format(3.141592653589*(self.radio*2), ".3f")

radio = 5
newFigura = Circulo(radio)
print("Area del circulo: ",newFigura.obtenerArea())
print("Perimetro del circulo: ",newFigura.obtenerPerimetro())