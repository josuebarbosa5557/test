"""
Escribir una clase en python para encontrar la validez de una cadena de
paréntesis, '(', ')', '{', '}', '['  '].
Los paréntesis deben aparecer en el orden correcto, por ejemplo "()" y "()[]{}" son validos,
pero "[)", "({[)]" y "{{{" son inválidos.
"""

import re

class CadenaValida:
    
    def __init__(self, cadena):
        self.cadena = cadena

    def cadenasConParentesis(self):
        validacionParentesis = re.findall("\([)]{1}", self.cadena)
        validacionCorchetes = re.findall("\[[]]{1}", self.cadena)
        validacionLlaves = re.findall("\{[}]{1}", self.cadena)
        r = []
        if validacionParentesis:
            r.append(validacionParentesis)
        
        if validacionCorchetes:
            r.append(validacionCorchetes)
        
        if validacionLlaves:
            r.append(validacionLlaves)
        
        print("---Cadenas aceptadas---")
        for i in r:
            print(i)

def main():
    cadena = input("introduce una cadena: ")
    # cadena = '()[]'
    AFD = CadenaValida(cadena)
    AFD.cadenasConParentesis()

if __name__ == "__main__":
    main()