"""
6-Definir una función inversa() que calcule la inversión de una cadena.
Por ejemplo la cadena "estoy probando" debería devolver la cadena "odnaborp yotse
"""

def inversa(string):
    lista = list(string)
    lista = lista[::-1]
    lista = "".join(lista)
    return lista

string = "estoy probando"

print(inversa(string))