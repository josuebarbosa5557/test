"""5- Escribir una función sum() y una función multip() que sumen y multipliquen
    respectivamente todos los números de una lista. Por ejemplo: sum([1,2,3,4])
    debería devolver 10 y multip([1,2,3,4]) debería devolver 24."""

def sum(nums):
	if len(nums) == 1: # caso base
		return nums[0]
	return nums[0] + sum(nums[1:])

def multip(nums):
	if len(nums) == 1: # caso base
		return nums[0]
	return nums[0] * multip(nums[1:])

# lista = [12,13,14,15,16]
lista = [2,0]
print(sum(lista))
print(multip(lista))