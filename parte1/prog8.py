"""8-Definir una función superposicion() que tome dos listas y devuelva True
    si tienen al menos 1 miembro en común o devuelva False de lo contrario.
    Escribir la función usando el bucle for anidado."""

def superposocion(lista1, lista2):
    for i in range(len(lista1)):
        for j in range(len(lista2)):
            if lista1[i] == lista2[j]:
                return True
    return False

lista1 = ["a", "b", "c"]
lista2 = ["d", "e", "f"]

print(superposocion(lista1, lista2))