"""2-Definir una función max_de_tres(), que tome tres números
    como argumentos y devuelva el mayor de ellos"""

def max_de_tres(m, n, o):
    if m == n and m == o:
        return "Numeros identicos"
    else:
        actualMayor = (n, m)[m>n]
        finalMayor = o if o > actualMayor else actualMayor
        return finalMayor

print(max_de_tres(66,66,66))