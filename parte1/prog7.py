"""
7-Definir una función es_palindromo() que reconoce palíndromos (es decir, palabras que tienen 
el mismo aspecto escritas invertidas), ejemplo: es_palindromo("radar") tendría que devolver True.
"""

def palindromo(palabra):
    reversa = palabra[::-1]
    if palabra == reversa:
	    return True
    else:
	    return False

palabra = "radar"
print(palindromo(palabra))
# anita la gorda lagartona no traga la droga latina