"""10-Definir un histograma procedimiento() que tome una lista de números
    enteros e imprima un histograma en la pantalla"""

def procedimiento(listaNumeros):
    for i in range(len(listaNumeros)):
        print("*"*listaNumeros[i])

listaNumeros = [8, 4, 2]
procedimiento(listaNumeros)