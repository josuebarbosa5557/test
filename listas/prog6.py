"""
Escribe una función que lea las palabras de un archivo de texto (texto.txt) y construya
una lista donde cada palabra es un elemento de la lista.
"""

def construirLista(archivo):
    elementos = []
    for linea in archivo:
        elementos = linea.split()
        # print(linea)
    return elementos

archivo = open('archivo.txt', 'r')
print(construirLista(archivo))