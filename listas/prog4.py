"""
A - Escribe una función llamada "duplicado" que tome una lista y devuelva True si tiene algún 
elemento duplicado. La función no debe modificar la lista.
B - Crear una función que genere una lista de 23 números aleatorios del 1 al 100 y comprobar 
con la función anterior si existen elementos duplicados.
"""
import random

def duplicado(lista):
    NumeroDuplicado = False
    if len(set(lista)) < len(lista):
        NumeroDuplicado = True
    return "Numeros duplicados: ", NumeroDuplicado

lista = [1,3,5,7,8]
aleatorios = [random.randint(0,100) for i in range(23)]

# print(aleatorios)

print(duplicado(lista))
print(duplicado(aleatorios))