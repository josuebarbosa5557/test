"""
Para comprobar si una palabra está en una lista se puede utilizar el operador "in", pero 
sería una búsqueda lenta, ya que busca a través de las palabras en orden.
Debido a que las palabras están en orden alfabético, podemos acelerar las cosas con una 
búsqueda de bisección (también conocida como búsqueda binaria), que es similar a lo que haces
cuando buscas una palabra en el diccionario.

Escribir una función llamada "bisect" que tome una lista ordenada y una palabra como objetivo, 
y nos devuelva el índice en el que se encuentra en la lista, en caso de no aparecer en la lista
devuelve "No se encontró la palabra".
"""

lista = ["cristina", "marcia", "marili", "karla", "paola", "sandy"]
palabra = "cristina"
min = 0
max = len(lista)

def bisect(lista, palabra, min, max):
    mitad = (min + max) // 2
    if palabra == lista[mitad]:
        return mitad
    else:
        if palabra < lista[mitad]:
            return bisect(lista, palabra, min, mitad-1)
        else:
            return bisect(lista, palabra, mitad+1, max)

print("lista ordenada: ", sorted(lista), "\nindex: ", bisect(sorted(lista), palabra, min, max))