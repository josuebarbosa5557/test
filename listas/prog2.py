"""
Escribe una función llamada "elimina" que tome una lista y elimine el primer y último elemento
de la lista y cree una nueva lista con los elementos que no fueron eliminados.
Luego escribe una función que se llame "media" que tome una lista y devuelva una nueva lista
que contenga todos los elementos de la lista anterior menos el primero y el último.
"""

def elimina(lista):
    for i in range(len(lista)):
        if i == 0 or i == (len(lista)-1):
            lista.pop(i)
    return lista

def media(lista):
    for i in range(len(lista)):
        if i == 0 or i == (len(lista)-1):
            lista.pop(i)
    return lista

lista = [10,20,30,40,50,"1","2","3","4","5"]

print(elimina(lista))
print(media(elimina(lista)))