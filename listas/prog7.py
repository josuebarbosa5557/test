"""
Escribe una función llamada "inversa" que busque todas las palabras inversas de una lista.
Ejemplo de palabras inversas: radar, oro, rajar, rallar, salas, somos, etc...
"""

def inversa(listaPalabras):
    palabrasInversas = []
    for palabra in listaPalabras:
        reversa = palabra[::-1]
        if palabra == reversa:
	        palabrasInversas.append(palabra)
    return palabrasInversas

listaPalabras = ["oso", "radar", "marili", "oro", "cristina", "ligar"]
print(inversa(listaPalabras))