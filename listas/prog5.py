"""
Escribe una función llamada "elimina_duplicados" que tome una lista y devuelva una nueva
lista con los elementos únicos de la lista original. No tienen porque estar en el mismo orden.
"""

def elimina_duplicados(lista):
    return set(lista)

lista = [5,5,1,2,5,2,8,2,2,8,8,9,9]

print(list(elimina_duplicados(lista)))