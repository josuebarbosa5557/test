"""
Escriba una función que tome una lista de números y devuelva la suma acumulada, es decir, 
una nueva lista donde el primer elemento es el mismo, el segundo elemento es la suma del 
primero con el segundo, el tercer elemento es la suma del resultado anterior con el siguiente 
elemento y así sucesivamente. Por ejemplo, la suma acumulada de [1,2,3] es [1, 3, 6].
"""
def acumulador(numeros):
    for i in range(len(numeros)):
        # print(numeros[i])
        # print(numerosAcumulados[i])
        numerosAcumulados.append(numeros[i]+numerosAcumulados[i])
    numerosAcumulados.pop(0)
    return numerosAcumulados

numeros = [1,2,3]
numerosAcumulados = [0]
print(acumulador(numeros))