"""
Escribe una función "ordenada" que tome una lista como parámetro y devuelva True si la lista
está ordenada en orden ascendente y devuelva False en caso contrario.
Por ejemplo, ordenada([1, 2, 3]) retorna True y ordenada([b, a]) retorna False.
"""

def ordenada(lista):
    orden = True
    i = 1
    while(i < len(lista)):
        if lista[i-1] > lista[i]:
            orden = False
        i += 1
    return orden

lista = ["a","b","c","d","e","f"]

print(ordenada(lista))