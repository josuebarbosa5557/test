# Project Title

Plan de desarrollo backend

## Getting Started

backend con python y django rest framework

conocer python y practicar haciendo algunos programas.
trabajar con repositorios y a conocer Git.
desarrollo de web API's con Django Rest Framework.
levantar un servidor y crear la infraestructura en AWS.
documentar la API con swagger.
crear Unit Test para la API.

* [Python](https://www.python.org) - programming language that lets you work quickly and integrate systems more effectively
* [Django](https://www.djangoproject.com) -  The web framework used


## Authors

* **Josue Barbosa** - *Initial work* - [PurpleBooth](https://github.com/JosueBarbosaBarrera)